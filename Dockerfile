# probably should use a better distro but litecoin uses glibc so it's easier to use this
FROM ubuntu:latest

ARG LITECOIN_VERSION=0.18.1


RUN apt update && apt install -y gnupg wget

WORKDIR /

RUN wget https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz
RUN wget https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-linux-signatures.asc

# https://download.litecoin.org/README-HOWTO-GPG-VERIFY-TEAM-MEMBERS-KEY.txt
# verify signatures
RUN gpg --keyserver keyserver.ubuntu.com --recv-key FE3348877809386C && gpg --fingerprint FE3348877809386C \
        && gpg --verify litecoin-${LITECOIN_VERSION}-linux-signatures.asc
# verify files
RUN grep $(sha256sum litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz) litecoin-${LITECOIN_VERSION}-linux-signatures.asc \
    && echo "Binaries verification successful" || (echo "Binaries have been tampered with!" && exit 1)
RUN tar -zxf litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz && mv litecoin-${LITECOIN_VERSION} litecoin


FROM ubuntu:latest
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#user
RUN groupadd -r litecoin && useradd --no-log-init -r -g litecoin litecoin
RUN mkdir /litecoin && mkdir /data && chown -R litecoin: /data
COPY --from=0 /litecoin /litecoin
WORKDIR /litecoin
VOLUME [ "/data" ]

USER litecoin

# taken from the man page
EXPOSE 9333 9332

ENTRYPOINT [ "/litecoin/bin/litecoind" ]
CMD [ "-datadir=/data" ]
