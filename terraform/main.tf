provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

module "iam" {
  source = "./iam"
  users = var.users
  groups = var.groups
  group_membership = var.group_membership
}