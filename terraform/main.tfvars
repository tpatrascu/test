users = [
  "Eugene",
  "Milo",
  "Abigail",
  "Aidan",
  "Santiago",
  "Felix",
  "Morgan",
]

groups = [
  "Developers",
  "Ops",
]

group_membership = {
  "Developers": [
    "Eugene",
    "Milo",
    "Abigail",
    "Aidan",
  ],
  "Ops": [
    "Santiago",
    "Felix",
    "Morgan",
  ]
}
