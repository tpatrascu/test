resource "aws_iam_user" "users" {
  for_each = toset( var.users )
  name     = each.key
}

resource "aws_iam_group" "groups" {
  for_each = toset( var.groups )
  name     = each.key
}

data "aws_caller_identity" "current" {}

# allows all users on this AWS account to assume these roles but only if their user/group policy also allows it
data "aws_iam_policy_document" "assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
}

resource "aws_iam_role" "roles" {
  for_each = toset( var.groups )
  name     = each.key
  assume_role_policy = data.aws_iam_policy_document.assume-role-policy.json
}

data "aws_iam_policy_document" "assumerole-group-policy" {
  for_each = toset( var.groups )
  statement {
    actions = ["sts:AssumeRole"]

    resources = [
      aws_iam_role.roles[each.key].arn
    ]
  }
}

resource "aws_iam_group_policy" "group-assume-role" {
  for_each = toset( var.groups )
  name     = "${each.key}-assume-role"
  group = each.key

  policy = data.aws_iam_policy_document.assumerole-group-policy[each.key].json
}

resource "aws_iam_group_membership" "group-membership" {
  depends_on = [
    aws_iam_user.users,
    aws_iam_group.groups
  ]
  for_each = var.group_membership
  name = "${each.key}-membership"
  group = each.key
  users = each.value
}

