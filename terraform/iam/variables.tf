variable users {
  description = "List of users"
  type        = list
  default     = []
}

variable groups {
  description = "List of groups"
  type        = list
  default     = []
}

variable group_membership {
  description = "Group to users mapping"
  type        = map
  default     = {}
}