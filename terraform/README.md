# Sources

I made this for another case assignment a while ago and incidentally it does exactly what you asked. It's all my original code. Don't remember where I got everything from, mainly terraform docs, some other modules from github, etc.

# Usage

Using:

```
Terraform v0.14.3
+ provider registry.terraform.io/hashicorp/aws v3.22.0
```

```
export AWS_ACCESS_KEY_ID=thekeyid
export AWS_SECRET_ACCESS_KEY=thekey

terraform init
terraform apply -var-file main.tfvars
```
